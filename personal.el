;;; package --- Summary
;;; Commentary:
(require 'package)

;;; Code:
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/"))


(package-initialize)

(mapc
 (lambda (package)
   (unless (package-installed-p package)
     (progn (message "installing %s" package)
            (package-refresh-contents)
            (package-install package))))
 '(paradox multiple-cursors emmet-mode flycheck dashboard multi-term yasnippet auto-complete web-mode projectile counsel-projectile magit exec-path-from-shell hemisu-theme gruvbox-theme aggressive-indent expand-region diff-hl ace-window tango-plus-theme dracula-theme smex restclient github-modern-theme darktooth-theme soft-morning-theme twilight-bright-theme org yaml-mode undo-tree avy))

(server-start)
(setq mac-command-modifier 'control)
(require 'god-mode)
(require 'yaml-mode)
(require 'smex)
(require 'multiple-cursors)
(require 'emmet-mode)
(require 'flycheck)
(require 'dashboard)
(require 'multi-term)
(require 'yasnippet)
(require 'auto-complete-config)
(require 'web-mode)
(require 'exec-path-from-shell)
(require 'expand-region)
(require 'restclient)

;; Default mode
(avy-setup-default)
(set-fringe-mode 1)
(global-diff-hl-mode 1)
(ivy-mode 1)
(projectile-mode)
(scroll-bar-mode -1)
(menu-bar-mode -1)
(tool-bar-mode -1)
(recentf-mode 1)
(ido-mode 1)
(electric-pair-mode 1)
(global-linum-mode t)
(yas-global-mode 1)
(global-flycheck-mode 1)
(global-auto-revert-mode 1)
(show-paren-mode 1)
(electric-indent-mode 1)
(global-aggressive-indent-mode 1)
(global-undo-tree-mode 1)
(global-auto-complete-mode t)
(setq god-exempt-major-modes nil)
(setq god-exempt-predicates nil)

;; Default variables
(add-to-list 'ac-modes 'sql-mode)
(add-to-list 'ac-modes 'restclient-mode)
(setq-default fringes-outside-margins t)
(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)
(setq-default linum-format " %3d  ")
(setq-default line-spacing 0)
(windmove-default-keybindings)
(counsel-projectile-mode 1)
(fset 'yes-or-no-p 'y-or-n-p)
(setq sml/no-confirm-load-theme t)
(add-to-list 'default-frame-alist '(width . 120))
(add-to-list 'default-frame-alist '(height . 40))
(setq auto-save-default nil)
(setq make-backup-files nil)
(diff-hl-flydiff-mode 1)
(add-hook 'before-save-hook 'whitespace-cleanup)
(add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)
(setq multi-term-program "/bin/bash")
(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))
(add-to-list 'auto-mode-alist '("\\.http\\'" . restclient-mode))
(setq electric-pair-pairs '(
                            (?\' . ?\')
                            (?\` . ?\`)
                            (?\{ . ?\})
                            ) )

;; Mode-line
(defun my-termmode ()
  (linum-mode -1)
  (setq mode-line-format nil))

(add-hook 'term-mode-hook 'my-termmode)
(add-hook 'paradox-menu-mode 'my-termmode)

;; Ido setup
(setq ido-enable-flex-matching t)
(setq ido-use-filename-at-point 'guess)
(setq ido-create-new-buffer 'always)
(setq ido-everywhere t)

;; God-mode
(add-hook 'god-mode-enabled-hook 'my-god-cursor)
(add-hook 'god-mode-disabled-hook 'my-human-cursor)

;; Ivy config
(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)
(global-set-key "\C-s" 'swiper)
(global-set-key (kbd "C-c C-r") 'ivy-resume)
(global-set-key (kbd "M-m") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)


;; Dashboard
(dashboard-setup-startup-hook)
(setq dashboard-banner-logo-title "La Codasia")
(setq dashboard-startup-banner "~/.emacs.d/banner.png")
(setq dashboard-items '((recents  . 5)
                        (projects . 5)))


;; Font
(set-face-attribute 'default nil :font "Overpass Mono 12")
(set-frame-font "Overpass Mono 12" nil t)


;; AutoComplete
(ac-config-default)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")

;; Emmet Config
(add-hook 'sgml-mode-hook 'emmet-mode)
(add-hook 'css-mode-hook 'emmet-mode)
(add-hook 'web-mode-hook 'emmet-mode)
(add-hook 'emmet-mode-hook (lambda () (setq emmet-indent-after-insert nil)))
(setq emmet-move-cursor-between-quotes t)
(setq emmet-expand-jsx-className? t)
(setq emmet-self-closing-tag-style " /")

;; Python Mode
(add-hook 'python-mode-hook
          (lambda ()
            (setq-default tab-width 4)
            (setq-default py-indent-tabs-mode t)
            (add-to-list 'write-file-functions 'delete-trailing-whitespace)))

;; WebMode/JSX Config
(defun my-web-mode-hook ()
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-enable-auto-pairing t)
  (setq web-mode-enable-css-colorization t)
  (setq web-mode-enable-current-element-highlight t))

(add-hook 'web-mode-hook  'my-web-mode-hook)
(add-to-list 'auto-mode-alist '("\\.js[x]?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.ts\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.json\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.scss\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.less\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))

(setq web-mode-content-types-alist
      '(("jsx" . "\\.js[x]?\\'")))

(defadvice web-mode-highlight-part (around tweak-jsx activate)
  (if (equal web-mode-content-type "jsx")
      (let ((web-mode-enable-part-face nil))
        ad-do-it)
    ad-do-it))


;; Flycheck config
(add-hook 'after-init-hook #'global-flycheck-mode)

(setq-default flycheck-disabled-checkers
              (append flycheck-disabled-checkers
                      '(javascript-jshint)))

(flycheck-add-mode 'javascript-eslint 'web-mode)

(setq-default flycheck-temp-prefix ".flycheck")

(setq-default flycheck-disabled-checkers
              (append flycheck-disabled-checkers
                      '(json-jsonlist)))

(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

(defun my/use-eslint-from-node-modules ()
  "Use local eslint if possible."
  (let* ((root (locate-dominating-file
                (or (buffer-file-name) default-directory)
                "node_modules"))
         (eslint (and root
                      (expand-file-name "node_modules/eslint/bin/eslint.js"
                                        root))))

    (when (and eslint (file-executable-p eslint))
      (setq-local flycheck-javascript-eslint-executable eslint))))
(add-hook 'flycheck-mode-hook #'my/use-eslint-from-node-modules)


;; ;; Ignoring electric indentation for python
;; (defun electric-indent-ignore-python (char)
;;   "Ignore electric indentation for python-mode"
;;   (if (equal major-mode 'python-mode)
;;       'no-indent
;;     nil))
;; (add-hook 'electric-indent-functions 'electric-indent-ignore-python)


;; typescript mode, yeah the very stupid typescript
(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1))
(setq company-tooltip-align-annotations t)
(add-hook 'before-save-hook 'tide-format-before-save)
(add-hook 'typescript-mode-hook #'setup-tide-mode)


;; Load other files
(load "~/.emacs.d/customs")
(load "~/.emacs.d/keybindings")

(provide 'personal)
;;; personal.el ends here
