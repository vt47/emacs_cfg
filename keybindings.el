;;; package --- Summary
;;; Commentary:
;;; Declare keybinds here
;;; Code:
(global-set-key (kbd "M-n") 'forward-paragraph)
(global-set-key (kbd "M-p") 'backward-paragraph)
(global-set-key (kbd "M-[") 'ace-window)
(global-set-key (kbd "C-c k") 'counsel-ag)
(global-set-key (kbd "C-c m") 'projectile-vc)
(global-set-key (kbd "C-c SPC") 'god-mode-all)

;;;; Cursor Movement
(global-set-key (kbd "C-c g") 'goto-line)
(global-set-key (kbd "M-<up>") 'move-line-up)
(global-set-key (kbd "M-<down>") 'move-line-down)

(global-set-key (kbd "C-.") 'mc/mark-next-like-this)
(global-set-key (kbd "C-,") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-;") 'projectile-find-file)
(global-set-key (kbd "C-q") 'emmet-expand-yas)
(global-set-key (kbd "M-<tab>") 'yas-expand)
(global-set-key (kbd "C-c t") 'counsel-load-theme)
(global-set-key "\C-c\C-n" 'end-of-line-and-indented-new-line)
(global-set-key "\C-c\C-p" 'previous-line-and-indented-new-line)
(global-set-key (kbd "C-v") 'delete-backward-char)
(global-set-key (kbd "M-v") 'backward-kill-word)

;;;; Shortcut function
(global-set-key (kbd "<f4>") 'split-window-horizontally)
(global-set-key (kbd "<f5>") 'split-window-vertically)
(global-set-key (kbd "<f6>") 'package-install)
(global-set-key (kbd "<f7>") 'recentf-open-files)
(global-set-key (kbd "<f8>") 'multi-term)
(global-set-key (kbd "<f9>") 'eval-buffer)
(global-set-key (kbd "C-=") 'er/expand-region)
(global-set-key (kbd "C-c C-k") 'copy-line)
(global-set-key (kbd "C-c b") 'switch-to-previous-buffer)
(global-set-key (kbd "C-/") 'undo)
(global-set-key (kbd "C-?") 'undo-tree-redo)
(global-set-key (kbd "C-l") 'kill-whole-line)
(global-set-key (kbd "C-c n") 'end-of-line-and-indented-new-line)
(global-set-key (kbd "C-c c") 'avy-goto-char)
(global-set-key (kbd "C-c j") 'avy-goto-char-2)
(global-set-key (kbd "C-c l") 'avy-goto-line)
(global-set-key (kbd "C-c f") 'avy-goto-char-in-line)
(global-set-key (kbd "C-c C-j") 'avy-resume)
(global-set-key (kbd "C-x k") 'volatile-kill-buffer)

;; Magit shortcut
(global-set-key (kbd "C-c C-m c f") 'magit-file-checkout)
(global-set-key (kbd "C-c C-m c b") 'magit-checkout)
(global-set-key (kbd "C-c C-m c n") 'magit-branch-and-checkout)


(provide 'keybindings)
;;; keybindings ends here
